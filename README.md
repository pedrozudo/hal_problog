# Note

This repo is not being worked on anymore. The most up-to-date code  is on this branch of ProbLog:
https://github.com/ML-KULeuven/problog/tree/dcproblog_develop

a short reamde can be found here:
https://github.com/ML-KULeuven/problog/tree/dcproblog_develop/problog/tasks/dcproblog

# HAL-ProbLog

The standard version of HAL-ProbLog works with the PSI-Solver. Follow the instruction below to get it running.

### Preliminary requirements ###
1. Set-up the [psipy](https://github.com/ML-KULeuven/psipy) python package that wraps around the PSI-Solver.

2. Make also sure the following libraries are installed:
```
pip install problog graphviz
```
Make sure you are using **python3**.

### Installation ###

Clone now the HAL-ProbLog repo to your designated directory (first command from below) and proceed as indicated.
```
git clone https://<username>@bitbucket.org/pedrozudo/hal_problog.git
cd hal_problog
python setup.py install
```

### How to run a program ###
You can now simply type for example:
```
hal_problog </path/to/hal_problog/repo>/examples/appears_tall.pl
```

In the examples directory you'll find files that you can have a peak at and that you can try to run.

Use:
```
hal_problog --help
```
to find out which flags you can use to run a program

### Weighted Model Integration
HAL-ProbLog has also been used to solve *weighted model Integration* problems and been embedded into the [pywmi](https://github.com/samuelkolb/pywmi) toolbox.

### GPU-accelerated sampling for HAL-ProbLog
Coming soon.
