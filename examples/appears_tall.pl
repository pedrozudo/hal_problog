2/5::male;3/5::female.

normal(180,8)~height:-male.
normal(160,8)~height:-female.
is_tall:-male, height~=Height, conS(180<Height).
is_tall:-female, height~=Height, conS(170<Height).
appears_tall:- is_tall.
3/10::appears_tall:-male.

query(is_tall).
query(appears_tall).
