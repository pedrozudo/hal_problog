n_clicks(5).
clicks(M,0):-n_clicks(N), between(0,N,M).
clicks(M,1):-n_clicks(N), between(0,N,M).


beta(1,1)~similarityAll.
S::sim(N):- similarityAll~=S.

beta(1,1)~b(N,0).
beta(1,1)~b(N,1).

B::cl(N,0):-  clicks(N,0), b(N,0)~=B.
B::cl(N,1):- clicks(N,1), sim(N), b(N,0)~=B.
B::cl(N,1):- clicks(N,1), \+sim(N), b(N,1)~=B.


evidence(cl(0,0), true).
evidence(cl(0,1), true).
evidence(cl(1,0), true).
evidence(cl(1,1), true).
evidence(cl(2,0), true).
evidence(cl(2,1), true).
evidence(cl(3,0), false).
evidence(cl(3,1), false).
evidence(cl(4,0), false).
evidence(cl(4,1), false).
evidence(cl(5,0), false).
evidence(cl(5,1), false).

:-free(similarityAll).
query(density(similarityAll)).
