def get_algebra(abe, values, free_variables):
    if abe=="psi":
        from .psi import PSI
        return PSI(values, free_variables)
    elif abe=="pyro":
        from .pyro import Pyro
        return PYRO(values)
    elif abe=="edward":
        from .edward import Edward
        return EDWARD(values)


SUB = str.maketrans("0123456789-", "₀₁₂₃₄₅₆₇₈₉₋")
M_SUB = "-".translate(SUB)

class Algebra(object):
    def __init__(self, values):
        self.density_values = values

    @staticmethod
    def name2str(name):
        name = "{term}{no_d}{M_SUB}{no_dim}".format(term=name[0],no_d=str(name[1]).translate(SUB),no_dim=str(name[2]).translate(SUB), M_SUB=M_SUB)
        for c, rc in {"(":"__", ")":"", ",":"_" }.items():
            name = name.replace(c, rc)
        return name

    def is_free(self, v):
        for fv in self.free_variables:
            if fv == v[0]:
                return True
        return False
