import psipy

from ..functions import pdfs, ValueExpr, ConditionExpr, ValueDimExpr
from .algebra import Algebra


class WeightPSI(object):
    def __init__(self, expression, variables, weighted_variables=set()):
        self.expression = expression
        self.variables = variables
        self.weighted_variables = weighted_variables

    def __str__(self):
        return psipy.toString(self.expression)

    def __repr__(self):
        return str(self.expression)


str2psi = {

    "<" : psipy.less,
    "=<" : psipy.less_equal,
    ">" : psipy.greater,
    "=>" : psipy.greater_equal,
    "=" : psipy.equal,
    "\=" : psipy.not_equal,
    "list" : list,

    "real" : psipy.real_symbol,

    "delta" : psipy.delta_pdf,
    "normal" : psipy.normal_pdf,
    "normalInd" : psipy.normalInd_pdf,
    "uniform" : psipy.uniform_pdf,
    "beta" : psipy.beta_pdf,
    "poisson" : psipy.poisson_pdf,

    "add" : psipy.add,
    "sub" : psipy.sub,
    "mul" : psipy.mul,
    "div" : psipy.div,
    "/" : psipy.div,
    "pow" : psipy.pow,
    "exp" : psipy.exp,
    "sigmoid" : psipy.sig
}

class PSI(Algebra):
    random_values = {}
    densities = {}
    normalization = False #TODO move this to argument of integration functions

    def __init__(self, values, free_variables):
        Algebra.__init__(self, values)
        self.free_variables = free_variables

    def S(self, symbol):
        if isinstance(symbol, (int, float)):
            return psipy.S(str(symbol))
        elif isinstance(symbol, str):
            return psipy.S(symbol)

    def add_simplify(self,a,b):
        return psipy.simplify(psipy.add(a,b))
    def mul_simplify(self,a,b):
        return psipy.simplify(psipy.mul(a,b))
    def div_simplify(self,a,b):
        return psipy.simplify(psipy.div(a,b))

    def one(self):
        return WeightPSI(self.S("1"),set())
    def zero(self):
        return WeightPSI(self.S("0"),set())
    def times(self, a, b, index=None):
        expression = psipy.mul(a.expression, b.expression)
        variables = a.variables.union(b.variables)
        weighted_variables = a.weighted_variables.union(b.weighted_variables)

        return WeightPSI(expression, variables, weighted_variables=weighted_variables)
    def plus(self, a, b, index=None):
        expression = psipy.add(a.expression, b.expression)
        variables = a.variables.union(b.variables)
        weighted_variables = a.weighted_variables.union(b.weighted_variables)
        return WeightPSI(expression, variables, weighted_variables=weighted_variables)
    def value(self, expression):
        algebraic_expression = self.construct_algebraic_expression(expression)
        algebraic_expression_variables = set(expression.variables)
        return WeightPSI(algebraic_expression, algebraic_expression_variables)
    def negate(self, weight):
        if isinstance(weight.expression, ConditionExpr):
            expression = psipy.negate_condition(weight.expression)
        else:
            expression = psipy.sub(psipy.S("1"), weight.expression)
        return WeightPSI(expression, weight.variables)
    def result(self, a, formula=None):
        weight = self.integrate(a)
        return weight

    def probability(self, a, z):
        expression = psipy.div(a.expression,z.expression)
        expression = psipy.simplify(expression)
        return WeightPSI(expression,a.variables)

    def integrate_tagged_variables(self, weight, int_tags, weight_tags):

        integrand = weight.expression
        vs = set(weight.variables)
        variables = int_tags.intersection(vs)

        wvs = set(weight.weighted_variables)

        for v in weight_tags:
            if v not in wvs:
                integrand = psipy.mul(integrand, self.densities[v])
                wvs.add(v)
        for v in variables:
            if v[:-1] in wvs and (self.normalization or not self.is_free(v)):
                var  = self.name2str(v)
                vs.remove(v)
                integrand = psipy.integrate([var], integrand)
        # integrand = psipy.simplify(integrand)
        return WeightPSI(integrand, vs, weighted_variables=wvs.union(weight.weighted_variables))

    def integrate(self, weight):
        integrant = weight.expression
        vs = set()

        # for rv in self.random_values:
        for rv in weight.variables:
            integrant = psipy.mul(integrant, self.densities[rv[:-1]])
        for rv in weight.variables:
        # for rv in self.random_values:
            if self.normalization or not self.is_free(rv[:-1]):
                integrant = psipy.integrate(self.random_values[rv[:-1]], integrant)
                # pass
            else:
                vs.add(rv)
        return WeightPSI(integrant, vs)


    def construct_density(self, functor, sym_names, args):
        if functor in (psipy.delta_pdf, psipy.normal_pdf,  psipy.uniform_pdf, psipy.beta_pdf, psipy.poisson_pdf):
            #sym_names has only one entry
            return functor(sym_names[0], *args)
        elif functor in (psipy.normalInd_pdf,):
            return functor(sym_names, *args)
        elif functor in (psipy.real_symbol, ):
            return psipy.S("1")
            # return functor(sym_names[0])


    def symbolize_name(self, dimension_values):
        sym_names = []
        for v in dimension_values:
            s = Algebra.name2str(v.functor)
            s = psipy.S(s)
            sym_names.append(s)
        return sym_names

    def construct_algebraic_expression(self, expression):
        if isinstance(expression, ValueExpr) and expression.functor in self.random_values:
            return self.random_values[expression.name]
        elif isinstance(expression.functor, (float, int)):
            return self.S(expression.functor)
        elif isinstance(expression, ValueDimExpr):
            density_name = expression.functor[:-1]
            dimension = expression.functor[-1]

            self.construct_algebraic_expression(self.density_values[density_name])
            algebraic_value  = self.random_values[density_name][dimension]
            return algebraic_value
        else:
            args = [self.construct_algebraic_expression(a) for a in expression.args]
            functor = str2psi[expression.functor]
            if expression.functor in pdfs:
                sym_names = self.symbolize_name(expression.dimension_values)
                density = self.construct_density(functor, sym_names, args)
                self.densities[expression.name] = density
                self.random_values[expression.name] = sym_names
                # return sym_name
            elif functor==list:
                return args
            else:
                result = functor(*args)
                return result
