from problog.engine_builtin import check_mode
from problog.logic import Term, Constant, term2list, list2term

from .formula import LogicFormulaHAL
from .functions import SymbolicExpr, ValueExpr, ConditionExpr, DensityExpr





def _builtin_density(term, args=(), target=None, engine=None, callback=None, transform=None, **kwdargs):
    check_mode( (term,), ['c'], functor='density_builtin')
    actions = []

    try:
        node_ids = target.density_nodes[term]
    except:
        raise ValueError("Cannot query density of discrete random variable ({}).".format(term))

    target.density_queries[term] = set()

    for nid in node_ids:
        if nid in target.density_node_body:
            body_node = target.density_node_body[nid]
        else:
            body_node = target.TRUE

        density_name =  target.get_density_name(term, nid)
        density = DensityExpr(density_name)
        density = Constant(density)
        target.add_name(density, body_node, target.LABEL_QUERY)

        target.density_queries[term].add(density)

    actions += callback.notifyComplete()
    return False, actions



def _builtin_free(free_variable, args=(), target=None, engine=None, callback=None, transform=None, **kwdargs):
    check_mode( (free_variable,), ['c'], functor='free_builtin')

    actions = []

    target.free_variables.add(free_variable)
    actions += callback.notifyResult((free_variable,), is_last=False)

    actions += callback.notifyComplete()
    return True, actions

def _builtin_free_list(free_variables, args=(), target=None, engine=None, callback=None, transform=None, **kwdargs):
    check_mode( (free_variables,), ['l'], functor='free_builtin')
    free_variables = term2list(free_variables)
    actions = []
    for v in free_variables:
        target.free_variables.add(v)
    actions += callback.notifyResult((free_variables,), is_last=False)

    actions += callback.notifyComplete()
    return True, actions


def get_value_type(value):
    if isinstance(value, int):
        return "point"
    else:
        return "vector"

def create_value(value, value_functor, value_args, value_name, value_type):
    if value_type=="point":
        dimensions = 1
        return ValueExpr(value_functor, value_args, value_name, dimensions)
    else:
        value_list  = term2list(value)
        dimensions = len(value_list)
        return ValueExpr(value_functor, value_args, value_name, dimensions)

def get_value_terms(value, value_type):
    value_terms = []
    for dv in value.dimension_values:
        value_terms.append(Constant(dv))
    if value_type=="point":
        value_terms = Constant(value_terms[0])
    else:
        value_terms = [vt for vt in value_terms]
        value_terms = list2term(value_terms)
    return value_terms

def _builtin_infix_valS(term, value, args=(), target=None, engine=None, callback=None, transform=None, **kwdargs):
    check_mode( (term,value), ['c*'], functor='~=')
    value_type = get_value_type(value)

    try:
        node_ids = target.density_nodes[term]
    except:
        raise ValueError("The value of a discrete random variable ({}) is not defined.".format(term))
    actions = []
    for nid in node_ids:
        node = target.get_node(nid)
        value_name =  target.get_density_name(term, nid)
        if value_name in target.density_values:
            value = target.density_values[value_name]
        else:
            probability = node.probability.functor
            value_functor = probability.functor
            value_args = [target.create_ast_representation(a) for a in probability.args]
            value = create_value(value, value_functor, value_args, value_name, value_type)
            target.density_values[value_name] = value

        value_terms = get_value_terms(value, value_type)
        term_pass = (term, value_terms)
        if nid in target.density_node_body:
            pass_node = target.density_node_body[nid]
        else:
            pass_node = 0
        actions += callback.notifyResult(term_pass, node=pass_node, is_last=False)

    actions += callback.notifyComplete()
    return True, actions

def _builtin_conS(condition, target=None, engine=None, callback=None, **kwdargs):
    check_mode( (condition,), ['c'], functor='conS_builtin')
    actions = []

    arg0 = target.create_ast_representation(condition.args[0])
    arg1 = target.create_ast_representation(condition.args[1])
    args = (arg0,arg1)
    functor = condition.functor.strip("'")
    symbolic_condition = ConditionExpr(functor, args)
    probability = Constant(symbolic_condition)
    hashed_symbolic = hash(symbolic_condition)
    con_node = target.add_atom(identifier=hashed_symbolic, probability=probability, name=Constant(str(condition)))

    actions += callback.notifyResult([condition], node=con_node, is_last=True, parent=None)
    return True, actions

def _builtin_addS(t1, t2, res, target=None, engine=None, callback=None, **kwdargs):
    check_mode( (t1,t2,res), ['**v'], functor='addS_builtin')
    actions = []
    symbolic_expr  = target.create_ast_representation(Term("add", t1,t2))
    term_pass = (t1, t2, Constant(symbolic_expr))
    actions += callback.notifyResult(term_pass, 0, False)
    actions += callback.notifyComplete()
    return True, actions

def _builtin_subS(t1, t2, res, target=None, engine=None, callback=None, **kwdargs):
    check_mode( (t1,t2,res), ['**v'], functor='subS_builtin')
    actions = []
    symbolic_expr  = target.create_ast_representation(Term("sub", t1,t2))
    term_pass = (t1, t2, Constant(symbolic_expr))
    actions += callback.notifyResult(term_pass, 0, False)
    actions += callback.notifyComplete()
    return True, actions

def _builtin_mulS(t1, t2, res, target=None, engine=None, callback=None, **kwdargs):
    check_mode( (t1,t2,res), ['**v'], functor='mulS_builtin')
    actions = []
    symbolic_expr  = target.create_ast_representation(Term("mul", t1,t2))
    term_pass = (t1, t2, Constant(symbolic_expr))
    actions += callback.notifyResult(term_pass, 0, False)
    actions += callback.notifyComplete()
    return True, actions

def _builtin_divS(t1, t2, res, target=None, engine=None, callback=None, **kwdargs):
    check_mode( (t1,t2,res), ['**v'], functor='divS_builtin')
    actions = []
    symbolic_expr = target.create_ast_representation(Term("div", t1,t2))
    term_pass = (t1, t2, Constant(symbolic_expr))
    actions += callback.notifyResult(term_pass, 0, False)
    actions += callback.notifyComplete()
    return True, actions

def _builtin_powS(t1, t2, res, target=None, engine=None, callback=None, **kwdargs):
    check_mode( (t1,t2,res), ['**v'], functor='powS_builtin')
    actions = []
    symbolic_expr  = target.create_ast_representation(Term("pow", t1,t2))
    term_pass = (t1, t2, Constant(symbolic_expr))
    actions += callback.notifyResult(term_pass, 0, False)
    actions += callback.notifyComplete()
    return True, actions

def _builtin_expS(t1, res, target=None, engine=None, callback=None, **kwdargs):
    check_mode( (t1,res), ['*v'], functor='expS_builtin')
    actions = []
    symbolic_expr  = target.create_ast_representation(Term("exp", t1))
    term_pass = (t1, Constant(symbolic_expr))
    actions += callback.notifyResult(term_pass, 0, False)
    actions += callback.notifyComplete()
    return True, actions

def _builtin_sigS(t1, res, target=None, engine=None, callback=None, **kwdargs):
    check_mode( (t1,res), ['*v'], functor='sigS_builtin')
    actions = []
    symbolic_expr  = target.create_ast_representation(Term("sig", t1))
    term_pass = (t1, Constant(symbolic_expr))
    actions += callback.notifyResult(term_pass, 0, False)
    actions += callback.notifyComplete()
    return True, actions
