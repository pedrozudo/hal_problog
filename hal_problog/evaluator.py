from collections import defaultdict

from problog.evaluator import Semiring, FormulaEvaluator

from .algebra.algebra import get_algebra



class SemiringHAL(Semiring):
    def __init__(self, neutral, abe, density_values, density_queries, free_variables, **kwdargs):
        Semiring.__init__(self)
        self.neutral = neutral
        self.density_queries = density_queries
        self.algebra = get_algebra(abe, density_values, free_variables)
        self.pos_values = {}
    def zero(self):
        return self.algebra.zero()
    def one(self):
        return self.algebra.one()
    def plus(self, a, b, index=None):
        return self.algebra.plus(a,b)
    def times(self, a, b, index=None):
        result = self.algebra.times(a,b)
        return result
    def negate(self, a):
        return self.algebra.negate(a)
    def pos_value(self, a, key, index=None):
        a = a.functor
        pv = self.value(a)
        self.pos_values[key] = pv
        return pv
    def neg_value(self, a, key, index=None):
        nv = self.pos_values[key]
        nv = self.negate(nv)
        return nv
    def value(self, a):
        return self.algebra.value(a)
    def result(self, evaluator, index, formula=None, normalization=False):
        a = evaluator.get_weight(index)
        return self.algebra.result(a, formula=formula)
    def is_dsp(self):
        return True
    def is_nsp(self):
        return False




class SemiringHALPInt(SemiringHAL):
    def __init__(self, neutral, abe, density_values, density_queries, free_variables, **kwdargs):
        self.tags = ({},{})
        SemiringHAL.__init__(self, neutral, abe, density_values, density_queries, free_variables, **kwdargs)
    def times(self, a, b, index=None):
        result = self.algebra.times(a,b)
        if index in self.tags[0]:
            result = self.algebra.integrate_tagged_variables(result, self.tags[0][index], self.tags[1].get(index,[]))
        return result
    def negate(self, a):
        return self.algebra.negate(a)
    def pos_value(self, a, key, index=None):
        a = a.functor
        pv = self.value(a)
        self.pos_values[key] = pv
        if index in self.tags[0]:
            pv = self.algebra.integrate_tagged_variables(pv, self.tags[0][index], self.tags[1].get(index,[]))
        return pv
    def neg_value(self, a, key, index=None):
        nv = self.pos_values[key]
        nv = self.negate(nv)

        if index and -index in self.tags[0]:
            nv = self.algebra.integrate_tagged_variables(nv, self.tags[0][-index], self.tags[1].get(-index,[]))
        return nv


class WeightSA(object):
    def __init__(self, variables=set(), common_variables_children=set()):
        self.variables = variables
        self.common_variables_children = common_variables_children

class WorldWeightTags(object):
    def __init__(self, variable_path_lengths={}, variables=[]):
        self.variable_path_lengths = variable_path_lengths
        self.variables = variables

class SemiringStaticAnalysis(Semiring):
    def __init__(self, neutral, abe, density_values, density_queries, free_variables, **kwdargs):
        Semiring.__init__(self)
        self.neutral = neutral
        self.density_values = density_values
        self.density_queries = density_queries
        self.algebra = get_algebra(abe, density_values, free_variables)

        self._fact_weights = {}
        self._computed_weights = {}
        self._formula = None

        self.pos_values = {}


        self._ww_fact_weights = {}
        self._ww_computed_weights = {}

        self.index2int_tags = {}
        self.index2weight_tags = {}

    def zero(self):
        return WeightSA()
    def one(self):
        return WeightSA()
    def plus(self, a, b, index=None):
        variables = a.variables.union(b.variables)
        common_variables_children = a.variables.intersection(b.variables)
        return WeightSA(variables, common_variables_children)
    def times(self, a, b, index=None):
        variables = a.variables.union(b.variables)
        common_variables_children = a.variables.intersection(b.variables)
        return WeightSA(variables, common_variables_children)
    def negate(self, a):
        return a
    def pos_value(self, a, key, index=None):
        a = self.value(a.functor)
        self.pos_values[key] = a
        return self.pos_values[key]
    def neg_value(self, a, key, index=None):
        a = self.pos_values[key]
        return a
    def value(self, a):
        variables = self.algebra.value(a).variables
        return WeightSA(variables, variables)
    def result(self, evaluator, index, formula=None, normalization=False):
        self._fact_weights = evaluator._fact_weights
        self._computed_weights = evaluator._computed_weights
        self._formula = evaluator.formula



        #first bottom up (computes weights for each node, which is then used in top down evaluation)
        a = evaluator.get_weight(index)

        #top down
        index2int_tags = {}
        self._tag_integration_nodes(index, index2int_tags, set()) #this takes index2int_tags and manipulates it
        self.index2int_tags = {k:v for k,v in index2int_tags.items() if index2int_tags[k]} #this removes entries in the dictionary that have emptyset values

        #second bottom up
        self.index2weight_tags = self._tag_weight_nodes(index)
        # self.index2weight_tags = {k:v for k,v in self.index2weight_tags.items() if self.index2weight_tags[k]}
        # #clean up
        self._fact_weights.clear()
        self._computed_weights.clear()
        self._ww_fact_weights.clear()
        self._ww_computed_weights.clear()
        self._formula  = None

        return self.index2int_tags, self.index2weight_tags.variable_path_lengths
    def is_dsp(self):
        return True
    def is_nsp(self):
        return False


    def ww_one(self):
        return WorldWeightTags()
    def ww_zero(self):
        return WorldWeightTags()
    def ww_plus(self, a, b, index=None):
        result = {}
        path_lengths_a = a.variable_path_lengths
        path_lengths_b = b.variable_path_lengths

        var_a = set(path_lengths_a.keys())
        var_b = set(path_lengths_b.keys())
        for v in var_a - var_b:
            result[v] = {}
            for node_id in path_lengths_a[v]:
                result[v].update({node_id: path_lengths_a[v][node_id]+1})
        for v in var_b - var_a:
            result[v] = {}
            for node_id in path_lengths_b[v]:
                result[v].update({node_id: path_lengths_b[v][node_id]+1})
        for v in var_a & var_b:
            result[v] = {}
            node_id_a = set(path_lengths_a[v].keys())
            node_id_b = set(path_lengths_b[v].keys())
            for nid in node_id_a - node_id_b:
                result[v].update({nid: path_lengths_a[v][nid]+1})
            for nid in node_id_b - node_id_a:
                result[v].update({nid: path_lengths_b[v][nid]+1})
            for nid in node_id_a & node_id_b:
                result[v].update({nid: max(path_lengths_a[v][nid], path_lengths_b[v][nid])+1})

        if index in self.index2int_tags:
            vs = [v[:-1] for v in self.index2int_tags[index] if v[:-1] not in path_lengths_a and v[:-1] not in path_lengths_b]
            for v in vs:
                result.update({v:{index:0}})

        return WorldWeightTags(variable_path_lengths=result)

    def ww_times(self, a, b, index=None):
        result = {}
        path_lengths_a = a.variable_path_lengths
        path_lengths_b = b.variable_path_lengths

        var_a = set(path_lengths_a.keys())
        var_b = set(path_lengths_b.keys())
        for v in var_a - var_b:
            result[v] = {}
            for node_id in path_lengths_a[v]:
                result[v].update({node_id: path_lengths_a[v][node_id]+1})
        for v in var_b - var_a:
            result[v] = {}

            for node_id in path_lengths_b[v]:
                result[v].update({node_id: path_lengths_b[v][node_id]+1})
        for v in var_a & var_b:
            result[v] = {}
            path_sum_a = sum(path_lengths_a[v].values())
            path_sum_b = sum(path_lengths_b[v].values())

            if path_sum_a>=path_sum_b:
                for node_id in path_lengths_a[v]:
                    result[v].update({node_id: path_lengths_a[v][node_id]+1})
            else:
                for node_id in path_lengths_b[v]:

                    result[v].update({node_id: path_lengths_b[v][node_id]+1})

        if index in self.index2int_tags:
            vs = [v[:-1] for v in self.index2int_tags[index] if v[:-1] not in path_lengths_a and v[:-1] not in path_lengths_b]
            for v in vs:
                result.update({v:{index:0}})

        return WorldWeightTags(variable_path_lengths=result)
    def ww_value(self, a, index):
        if index in self.index2int_tags:
            variable_path_lengths = {v[:-1]:{index:0} for v in a.variables if v in self.index2int_tags[index]}
            return WorldWeightTags(variable_path_lengths=variable_path_lengths, variables=a.variables)
        else:
            return WorldWeightTags(variables = a.variables)
    def ww_negate(self,a, index):
        result = {}
        variables = a.variable_path_lengths.keys()

        if index in self.index2int_tags:
            vs = [v[:-1] for v in self.index2int_tags[index] if v[:-1] not in a.variables]
            for v in vs:
                result.update({v:{index:0}})

        return WorldWeightTags(variable_path_lengths=result)


    #this is depth first. optimization would be breath first
    def _tag_integration_nodes(self, index, index2tags, intersection_parents):
        if index == self._formula.TRUE:
            return
        elif index == self._formula.FALSE:
            return
        else:
            if abs(index) in self._fact_weights:
                weight = self._fact_weights[abs(index)]
                if index<0:
                    weight = weight[1]
                else:
                    weight = weight[0]
            elif abs(index) in self._computed_weights:
                assert index>0
                weight = self._computed_weights[abs(index)]

            node = self._formula.get_node(abs(index))
            ntype = type(node).__name__

            if ntype=="atom" or ntype=="conj":
                if not index in index2tags:
                    index2tags[index] = weight.variables
                index2tags[index].difference_update(intersection_parents)
            if ntype=="conj" or ntype=="disj":
                node = self._formula.get_node(index)

                if ntype=="conj":
                    intersection_parents = intersection_parents.union(weight.common_variables_children)

                for c in node.children:
                    self._tag_integration_nodes(c, index2tags, intersection_parents)





    #tells us in which node to include the world weight, i.e. a density or polynomial
    def _tag_weight_nodes(self, index):
        return self._get_world_weight_tags(index)

    def _compute_world_weight_tags(self, index):
        """Compute the weight of the node with the given index.

        :param index: integer or formula.TRUE or formula.FALSE
        :return: weight of the node
        """
        if index == self._formula.TRUE:
            return self.ww_one()
        elif index == self._formula.FALSE:
            return self.ww_zero()
        else:
            node = self._formula.get_node(abs(index))
            ntype = type(node).__name__
            if ntype == 'atom':

                return self.ww_one()
            else:
                childprobs = [self._get_world_weight_tags(c) for c in node.children]
                if ntype == 'conj':
                    assert len(childprobs) == 2
                    p = self.ww_times(*childprobs, index=index)
                    return p
                elif ntype == 'disj':
                    assert len(childprobs) >= 2
                    p = childprobs[0]
                    for cp in childprobs[1:]:
                        p = self.ww_plus(p, cp, index=index)
                    return p
                else:
                    raise TypeError("Unexpected node type: '%s'." % ntype)

    def _get_world_weight_tags(self, index):
        """Get the weight of the node with the given index.

        :param index: integer or formula.TRUE or formula.FALSE
        :return: weight of the node
        """

        if index == self._formula.TRUE:
            return self.ww_one()
        elif index == self._formula.FALSE:
            return self.ww_zero()
        elif index < 0:
            weight = self._ww_fact_weights.get(abs(index))
            if weight is None:
                # This will only work if the semiring support negation!
                pw = self._get_world_weight_tags(-index)
                return self.ww_negate(pw, index)
            else:
                # variable_path_lengths = {v:{index:0} for v in weight[1].variables}
                return self.ww_value(weight[1], index)
        else:
            weight = self._fact_weights.get(index)
            if weight is None:
                weight = self._ww_computed_weights.get(index)
                if weight is None:
                    weight = self._compute_world_weight_tags(index)
                    self._ww_computed_weights[index] = weight
                return weight
            else:
                return self.ww_value(weight[0], index)




class FormulaEvaluatorHAL(FormulaEvaluator):
    def __init__(self, formula, semiring, weights=None):
        FormulaEvaluator.__init__(self, formula, semiring, weights=None)

    def evaluate(self, index, normalization=False):
        result =  self.semiring.result(self, index, formula=self.formula)
        return result

    def compute_weight(self, index):
        """Compute the weight of the node with the given index.

        :param index: integer or formula.TRUE or formula.FALSE
        :return: weight of the node
        """
        if index == self.formula.TRUE:
            return self.semiring.one()
        elif index == self.formula.FALSE:
            return self.semiring.zero()
        else:
            node = self.formula.get_node(abs(index))
            ntype = type(node).__name__
            if ntype == 'atom':
                return self.semiring.one()
            else:
                childprobs = [self.get_weight(c) for c in node.children]
                if ntype == 'conj':
                    assert len(childprobs) == 2
                    p = self.semiring.times(*childprobs, index=index)
                    return p
                elif ntype == 'disj':
                    assert len(childprobs) >= 2
                    p = childprobs[0]
                    for cp in childprobs[1:]:
                        p = self.semiring.plus(p, cp, index=index)
                    return p
                else:
                    raise TypeError("Unexpected node type: '%s'." % ntype)

    def get_weight(self, index):
        """Get the weight of the node with the given index.

        :param index: integer or formula.TRUE or formula.FALSE
        :return: weight of the node
        """

        if index == self.formula.TRUE:
            return self.semiring.one()
        elif index == self.formula.FALSE:
            return self.semiring.zero()
        elif index < 0:
            weight = self._fact_weights.get(abs(index))
            if weight is None:
                # This will only work if the semiring support negation!
                nw = self.get_weight(-index)
                return self.semiring.negate(nw)
            else:
                return weight[1]
        else:
            weight = self._fact_weights.get(index)
            if weight is None:
                weight = self._computed_weights.get(index)
                if weight is None:
                    weight = self.compute_weight(index)
                    self._computed_weights[index] = weight
                return weight
            else:
                return weight[0]


class FormulaEvaluatorHALStaticAnalysis(FormulaEvaluatorHAL):
    def __init__(self, formula, semiring, weights=None):
        FormulaEvaluatorHAL.__init__(self, formula, semiring, weights=None)
