from problog.logic import Term, Constant

pdfs = {
    'real' : 'real',
    
    'delta' : 'delta,',
    'normal' : 'normal',
    'normalInd' : 'normalInd',
    'beta' : 'beta',
    'poisson' : 'poisson',
    'uniform' : 'uniform',
    'catuni' : 'catuni'
}

cdfs = {
    'sigmoid' : 'sigmoid'
}

infix_functors = ["/"]
comparison_functors = ["<", ">", "=<", "=>"]

class SymbolicExpr(object):
    def __init__(self, functor, args):
        self.functor = functor
        self.args = args
        self.variables = set()
        for a in args:
            self.variables = self.variables.union(a.variables)

    def __str__(self):
        args = list(map(str,self.args))
        if not args:
            return str(self.functor)
        elif self.functor in infix_functors:
            return "{arg0}{functor}{arg1}".format(functor=self.functor,arg0=args[0], arg1=args[1])
        elif self.functor in comparison_functors:
            return "{arg0}{functor}{arg1}".format(functor=self.functor,arg0=args[0], arg1=args[1])
        elif self.functor=="list":
            return "["+",".join(map(str, self.args))+"]"
        else:
            args = ",".join(args)
            return "{functor}({args})".format(functor=self.functor,args=args)
    def __repr__(self):
        return str(self)


class ConditionExpr(SymbolicExpr):
    def __init__(self, functor, args):
        SymbolicExpr.__init__(self, functor, args)


class ValueDimExpr(SymbolicExpr):
    def __init__(self, value):
        SymbolicExpr.__init__(self, value, ())
        self.functor = value
        self.variables = (value,)

    def __str__(self):
        return "({},{},{})".format(self.functor[0], self.functor[1], self.functor[2])

    def __repr__(self):
        return str(self)




class ValueExpr(object):
    def __init__(self, functor, args, name, dimensions):
        self.functor = functor
        self.args = args
        self.name = name
        self.dimensions = dimensions
        self.dimension_values = self.make_dim_values(self.dimensions)

    def make_dim_values(self, dimensions):
        dimension_values = []
        for d in range(0,dimensions):
            dim_name = self.name+(d,)
            dimension_values.append(ValueDimExpr(dim_name))
        return dimension_values

    def __str__(self):
        return "({},{})".format(self.name[0], self.name[1])

    def __repr__(self):
        return str(self)


class DensityExpr(object):
    def __init__(self, density_tuple):
        self.density = density_tuple[0]
        self.id = density_tuple[1]
    def __str__(self):
        return str((self.density, self.id))

    def __repr__(self):
        return str(self)
