:-use_module(library(lists)).


density(Q):- possible(Q), density_builtin(Q).
free(X):-free_builtin(X).
free_list(X):-free_list_builtin(X).

Var~=Val:- possible(Var), infix_valS_builtin(Var, Val).

conS(Condition):-conS_builtin(Condition).

addS(T1,T2,Res) :- addS_builtin(T1,T2,Res).
subS(T1,T2,Res) :- subS_builtin(T1,T2,Res).
mulS(T1,T2,Res) :- mulS_builtin(T1,T2,Res).
divS(T1,T2,Res) :- divS_builtin(T1,T2,Res).
powS(Base,Exponent,Res) :- powS_builtin(Base,Exponent,Res).
expS(Exponent,Res) :- expS_builtin(Exponent,Res).
sigS(X,Res) :- sigS_builtin(X,Res).



% cat_uni_list(X,Y,L):-select_uniform(X, L, Y, R). %check first whether list or integer
% cat_uni(Key,X,N):-findall(M,between(1,N,M), L), select_uniform(Key, L, X, R).
% %cat(X,W,L):-select_weighted(some_key, W, L, X, R).
%
% squery(X,P,E) :- squery_builtin(X,P,E).
% squery(X,P) :- squery_builtin(X,P).
