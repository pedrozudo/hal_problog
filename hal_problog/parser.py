from problog.parser import DefaultPrologParser, Token

class HALPrologParser(DefaultPrologParser):
    def __ini__(self):
        DefaultPrologParser.__init__(self, factory)

    def _token_tilde(self, s, pos):
        if s[pos:pos + 3] == '~==':
            return Token('~==', pos, binop=(700, 'xfx', self.factory.build_binop),
                         functor=self._next_paren_open(s, pos)), pos + 3
        elif s[pos:pos + 4] == '~=/=':
            return Token('~=/=', pos, binop=(700, 'xfx', self.factory.build_binop),
                         functor=self._next_paren_open(s, pos)), pos + 4
        elif s[pos:pos + 2] == '~<':
            return Token('~<', pos, binop=(700, 'xfx', self.factory.build_binop),
                         functor=self._next_paren_open(s, pos)), pos + 2
        elif s[pos:pos + 3] == '~=<':
            return Token('~=<', pos, binop=(700, 'xfx', self.factory.build_binop),
                         functor=self._next_paren_open(s, pos)), pos + 3
        elif s[pos:pos + 3] == '~>=':
            return Token('~>=', pos, binop=(700, 'xfx', self.factory.build_binop),
                         functor=self._next_paren_open(s, pos)), pos + 3
        elif s[pos:pos + 2] == '~>':
            return Token('~>', pos, binop=(700, 'xfx', self.factory.build_binop),
                         functor=self._next_paren_open(s, pos)), pos + 2
        elif s[pos:pos + 2] == '~=':
            return Token('~=', pos, binop=(700, 'xfx', self.factory.build_binop),
                        #  unop=(200, 'fy', self.factory.build_unop),
                         #this was added
                         functor=self._next_paren_open(s, pos)), pos + 2
        else:
            return Token('~', pos, unop=(900, 'fx', self.factory.build_unop),
                         binop=(1000, 'xfx', self.factory.build_probabilistic)), pos + 1
