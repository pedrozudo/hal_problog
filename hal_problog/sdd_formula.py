from collections import defaultdict

from problog.core import transform, transform_create_as
from problog.dd_formula import build_dd
from problog.sdd_formula import SDD
from problog.formula import LogicFormula


from .formula import atom, LogicFormulaHAL
from .dd_formula import DDEvaluatorHAL

try:
    import sdd
except Exception as err:
    sdd = None

class SDDHAL(SDD):
    def __init__(self, **kwdargs):
        SDD.__init__(self, **kwdargs)
        self.density_values = {}

    def _create_atom(self, identifier, probability, group, name=None, source=None):
        index = len(self) + 1
        var = self.get_manager().add_variable()
        self.atom2var[index] = var
        self.var2atom[var] = index
        return atom(identifier, probability, group, name, source)

    def _create_evaluator(self, semiring, weights, **kwargs):
        return DDEvaluatorHAL(self, semiring, weights, **kwargs)

    def to_formula(self, sdds):
        """Extracts a LogicFormula from the SDD."""
        formula = LogicFormulaHAL(keep_order=True)
        formula.density_values = self.density_values
        for n, q, l in self.labeled():
            node = self.get_inode(q)
            node = sdds[n]
            constraints = self.get_constraint_inode()
            nodec = self.get_manager().conjoin(node, constraints)
            i = self._to_formula(formula, nodec, {})
            formula.add_name(n, i, l)
        return formula

    def to_dot(self, *args, sdds=None, **kwargs):
        if kwargs.get('use_internal'):
            for qn, qi in self.queries():
                filename = mktempfile('.dot')
                self.get_manager().write_to_dot(self.get_inode(qi), filename)
                with open(filename) as f:
                    return f.read()
        else:
            return self.to_formula(sdds).to_dot(*args, **kwargs)


    def _to_formula(self, formula, current_node, cache=None):
        if cache is not None and int(current_node) in cache:
            return cache[int(current_node)]
        if self.get_manager().is_true(current_node):
            retval = formula.TRUE
        elif self.get_manager().is_false(current_node):
            retval = formula.FALSE
        elif sdd.sdd_node_is_literal(current_node):  # it's a literal
            lit = sdd.sdd_node_literal(current_node)
            at = self.var2atom[abs(lit)]
            node = self.get_node(at)
            if lit < 0:
                retval = -formula.add_atom(-lit, probability=node.probability, name=node.name, group=node.group)
            else:
                retval = formula.add_atom(lit, probability=node.probability, name=node.name, group=node.group)
        else:  # is decision
            size = sdd.sdd_node_size(current_node)
            elements = sdd.sdd_node_elements(current_node)
            primes = [sdd.sdd_array_element(elements, i) for i in range(0, size * 2, 2)]
            subs = [sdd.sdd_array_element(elements, i) for i in range(1, size * 2, 2)]

            # Formula: (p1^s1) v (p2^s2) v ...
            children = []
            for p, s in zip(primes, subs):
                p_n = self._to_formula(formula, p, cache)
                s_n = self._to_formula(formula, s, cache)
                c_n = formula.add_and((p_n, s_n))
                children.append(c_n)
            retval = formula.add_or(children)
        if cache is not None:
            cache[int(current_node)] = retval
        return retval



@transform(LogicFormula, SDDHAL)
def build_sdd(source, destination, **kwdargs):
    result = build_dd(source, destination, **kwdargs)
    return result

transform_create_as(SDDHAL, LogicFormula)
