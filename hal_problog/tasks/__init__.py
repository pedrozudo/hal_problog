import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), '..', '..'))

from problog.util import load_module
from hal_problog import version

problog_tasks = {}
problog_tasks['inference'] = 'hal_problog.tasks.inference'

problog_default_task = 'inference'


def load_task(name):
    """Load the module for executing the given task.

    :param name: task name
    :type name: str
    :return: loaded module
    :rtype: module
    """
    return load_module(problog_tasks[name])

def run_task(argv):
    """Execute a task in ProbLog.
    If the first argument is a known task name, that task is executed.
    Otherwise the default task is executed.

    :param argv: list of arguments for the task
    :return: result of the task (typically None)
    """
    if len(argv) > 0 and argv[0] in problog_tasks:
        task = argv[0]
        args = argv[1:]
    else:
        task = problog_default_task
        args = argv
    return load_task(task).main(args)


def main():
    argv = sys.argv[1:]
    run_task(argv)

if __name__ == '__main__':
    main()
