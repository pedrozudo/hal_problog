import argparse

import hal_problog.utils as utils
from hal_problog.solver import InferenceSolver, InferenceSolverPINT



def argparser(args):
    parser = argparse.ArgumentParser()

    parser.add_argument("file_name", type=str, help="HAL-ProbLog file")
    parser.add_argument("-draw_diagram", help="draw and save SDD diagram .gv/.pdf", action="store_true")
    parser.add_argument("-dpath", type=str, help="where to save diagram")

    if "-psi" in args:
        parser.add_argument("-psi", help="use the PSI-Solver (default)", dest="abe", const="psi" , action="store_const")
        parser.add_argument("-no_pint", help="switch partial integration off", action="store_true")
    elif "-pyro" in args:
        parser.add_argument("-pyro", help="use the pyro library", dest="abe", const="pyro" , action="store_const")
        parser.set_defaults(ttype="float32")
        parser.set_defaults(samples=2000)
    else:
        parser.add_argument("-psi", help="use the PSI-Solver (default)", dest="abe", const="psi" , action="store_const")
        parser.add_argument("-no_pint", help="carry out partial integration", action="store_true")
        parser.set_defaults(abe="psi")

    return parser


def main(args):
    parser = argparser(args)
    args = parser.parse_args(args)
    args = vars(args)
    program = utils.load_file(args['file_name'])

    if args["abe"]=="psi":
        args["pint"] = not(args["no_pint"])
        args.pop("no_pint")

    if args["abe"]=="psi" and args["pint"]:
        solver = InferenceSolverPINT(**args)
    else:
        solver = InferenceSolver(**args)
    probabilities = solver.probability(program, **args)
    solver.print_result(probabilities)
