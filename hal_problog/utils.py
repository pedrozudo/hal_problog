import os

from collections import OrderedDict

from problog.program import PrologString
from problog.logic import Term
from problog.engine import DefaultEngine
from problog.program import DefaultPrologFactory

from .parser import HALPrologParser


def consult_halpl(model_string):
    library_path = os.path.dirname(__file__)
    directive_consult_halpl = ":- consult('{}').\n".format(os.path.join(library_path,'library','hal.pl'))
    model_string = directive_consult_halpl + model_string
    return model_string

def load_file(model_file):
    with open(model_file) as f:
        model_string = f.read()
    program = load_string(model_string)
    return program

def load_string(model_string):
    factory = DefaultPrologFactory(identifier=0)
    parser = HALPrologParser(factory=factory)
    model_string = consult_halpl(model_string)
    program = PrologString(model_string, parser=parser, factory=factory)
    return program




# import inspect
# curframe = inspect.currentframe()
# calframe = inspect.getouterframes(curframe, 2)
# print('caller name:', calframe[1][1:4])
