import sys
import os

if sys.version_info[0]<3:
    sys.exit('Use python3 (it has been around for more than 10 years)!')

version_file = os.path.join(os.path.abspath(os.path.dirname(__file__)), "hal_problog/version.py")
version = {}
with open(version_file) as fp:
    exec(fp.read(), version)
version = version['version']


if __name__ == '__main__' and len(sys.argv) == 1:
    from problog import setup as problog_setup
    problog_setup.install()
elif __name__ == '__main__':
    from setuptools import setup, find_packages
    from setuptools.command.install import install


    class HALProbLogInstall(install):
        def run(self):
            install.run(self)

    package_data = {
        'hal_problog': [
            'library/hal.pl',
        ]
    }

    setup(
        name='hal_problog',
        version=version,
        python_requires='>3.5.2',
        description='HAL-ProbLog: Hybrid Algebraic ProbLog python library',
        url='https://bitbucket.org/pedrozudo/hal_problog',
        author='Pedro Zuidberg Dos Maritres',
        author_email='pedro.zuidbergdosmartires@cs.kuleuven.be',
        license='Apache Software License',
        classifiers=[
            'Development Status :: 0 - Beta',
            'License :: OSI Approved :: Apache Software License',
            'Intended Audience :: Science/Research',
            'Programming Language :: Python :: 3',
            'Programming Language :: Prolog',
            'Programming Language :: ProbLog2',
            'Topic :: Scientific/Engineering :: Artificial Intelligence',
        ],
        keywords='prolog hybrid probabilistic logic',

        entry_points={
            'console_scripts': ['hal_problog=hal_problog.tasks:main']
        },
        package_data=package_data,
        cmdclass={
            'install': HALProbLogInstall
        },
        setup_requires=[
            'graphviz',
            'problog',
        ],
        install_requires=[
            'graphviz',
            'problog',
        ],
        packages=find_packages()
    )
